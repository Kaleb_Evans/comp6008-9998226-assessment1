﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Math_Problem_1
{
    public partial class MainPage : ContentPage
    {

        public int RecievedFromXAML;

        public MainPage()
        {
            InitializeComponent();
        }

        public void OnButtonClicked(object sender, EventArgs e)
        {
            var clicked = new InputClass1(UserInput.Text);

            var result = clicked.SumOfValues();

            ResultsLabel.Text = ($"{result}");

        }
    }
}
