﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Math_Problem_1
{
    public class InputClass1
    {
        private string Input;

        public InputClass1(string _input)
        {
            Input = _input;
        }

        public int SumOfValues()
        {
            var sum = 0;
            var output = 0;

            bool isInt = int.TryParse(Input, out output);

            if (isInt)
            {
                for (int i = 0; i < output; i++)
                {
                    if (i % 3 == 0 || i % 5 == 0)
                    {
                        sum = sum + i;
                    }
                }
            }
            else
            {
                sum = 6969;
            }

            return sum;
        }

    }

}

