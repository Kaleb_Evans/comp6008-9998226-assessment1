﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Random_Number_game
{

    public partial class MainPage : ContentPage
    {

        int correct = 0;
        int total = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        public void OnButtonClicked(object sender, EventArgs e)
        {

            int output;

            //checking entry is number
            if (int.TryParse(UserInput.Text, out output))
            {
                bool result = InputClass.ranGame(output);

                if (result == true)
                {
                    //change colour green

                    labelColor.BackgroundColor = Color.Green;

                    labelTag.Text = "Got it right bro";
                    correct++;
                }
                else
                {
                    //change colour red
                    labelColor.BackgroundColor = Color.Red;

                    labelTag.Text = "Got it wrong bro";
                }


                total++;

                //label total text

                labelTotal.Text = total.ToString();
                labelCorrect.Text = correct.ToString();

            }
        }

    }
}
